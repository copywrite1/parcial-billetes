
print("Cambio Cheque - Billetes")
monto = int(input("Ingrese el monto de su cheque: $"))

billetes = [100000,50000,20000,10000,5000,2000,1000]
#Iteramos todos los villetes
for billete in billetes:
    if monto >= billete:
        # Dividimos y obtemenos el valor entero
        residuo = monto // billete
        print("Se entregan ", residuo , " billetes de $" , billete , " pesos colombianos")
        # Actualizamos el monto resultante
        monto = monto % billete

print("Quedan $", monto, " pesos")